import { useState } from "react";

import {
  Box,
  Container,
  Link,
  FormControl,
  TextField,
  InputAdornment,
  ButtonBase,
  Badge,
  Select,
  MenuItem,
} from "@mui/material";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined";
import ShoppingBagOutlinedIcon from "@mui/icons-material/ShoppingBagOutlined";
import MenuOutlinedIcon from "@mui/icons-material/MenuOutlined";

import "./MuiContainerHeader.css";

const MuiContainerHeader = () => {
  const [categoryValue, setCategoryValue] = useState("All Categories");

  const categoryChangeHandler = (event) => {
    setCategoryValue(event.target.value);
  };

  return (
    <Box
      sx={{
        zIndex: 2,
        position: "relative",
        boxShadow: "none",
        marginTop: '12px'
      }}
    >
      <Box
        sx={{
          zIndex: 3,
          position: "relative",
          height: "80px",
          transition: "height 250ms ease-in-out",
          background: "#fff",
        }}
      >
        <Container
          sx={{
            display: "flex",
            justifyContent: "space-between",
            gap: "16px",
            alignItems: "center"
          }}
        >
            <Box className="headerLogoBox"
              sx={{
                display: "flex",
                marginRight: "16px",
                alignItems: "center",
                minWidth: "170px",
                paddingTop: '10px'
              }}
            >
              <Link href="/">
                <img
                  src="https://bazar-react.vercel.app/assets/images/logo2.svg"
                  alt="logo"
                />
              </Link>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                flex: "1 1 0",
                paddingTop: "10px",
              }}
            >
              <Box
                sx={{
                  position: "relative",
                  flex: "1 1 0",
                  marginLeft: "auto",
                  marginRight: "auto",
                  maxWidth: "670px",
                }}
              >
                <FormControl
                  sx={{
                    width: "100%",
                  }}
                >
                  <TextField
                    className="inputRounded"
                    placeholder="Searching for..."
                    variant="outlined"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SearchOutlinedIcon />
                        </InputAdornment>
                      ),
                      endAdornment: (
                        <InputAdornment position="end">
                          {/* <ButtonBase>
                          All Categories
                          <KeyboardArrowDownOutlinedIcon />
                        </ButtonBase> */}
                          <Select
                            label="All Categories"
                            value={categoryValue}
                            onChange={categoryChangeHandler}
                            sx={{
                              backgroundColor: "#F6F9FC",
                              color: "#4B566B",
                              marginRight: "-14px",
                              borderTopRightRadius: "300px",
                              borderTopLeftRadius: "0px !important",
                              borderBottomRightRadius: "300px",
                              borderBottomLeftRadius: "0px !important",
                              borderLeft: "1px solid #DAE1E7 !important",
                              padding: "8px",
                            }}
                          >
                            <MenuItem value="All Categories">
                              All Categories
                            </MenuItem>
                            <MenuItem value="Car">Car</MenuItem>
                            <MenuItem value="Clothes">Clothes</MenuItem>
                            <MenuItem value="Electronics">Electronics</MenuItem>
                            <MenuItem value="Laptop">Laptop</MenuItem>
                            <MenuItem value="Desktop">Desktop</MenuItem>
                            <MenuItem value="Camera">Camera</MenuItem>
                            <MenuItem value="Toys">Toys</MenuItem>
                          </Select>
                        </InputAdornment>
                      ),
                    }}
                  />
                </FormControl>
              </Box>
            </Box>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                paddingTop: '10px'
              }}
            >
              <ButtonBase className='headerRightBtn'
                sx={{
                  padding: "10px",
                  backgroundColor: "#F3F5F9",
                  color: "rgba(0, 0, 0, 0.54)",
                  borderRadius: "50%",
                  textAlign: "center",
                  flex: "0 0 auto",
                  overflow: "visible",
                  alignItems: "center",
                }}
              >
                <PersonOutlinedIcon />
              </ButtonBase>
              <Badge className='headerRightBtn'>
                <ButtonBase
                  sx={{
                    marginLeft: "20px",
                    padding: "10px",
                    backgroundColor: "#F3F5F9",
                    borderRadius: "50%",
                    color: "rgba(0, 0, 0, 0.54)",
                  }}
                >
                  <Badge badgeContent={3} color="error">
                    <ShoppingBagOutlinedIcon />
                  </Badge>
                </ButtonBase>
              </Badge>
            <ButtonBase className='headerExpandMoreBtn' sx={{
            padding: '8px',
            borderRadius: '50%',
            border: '1px solid rgb(218, 225, 231)',
            color: 'rgba(0, 0, 0, 0.54)'
          }}>
            <MenuOutlinedIcon></MenuOutlinedIcon>
          </ButtonBase>
            </Box>
        </Container>
      </Box>
    </Box>
  );
};

export default MuiContainerHeader;
