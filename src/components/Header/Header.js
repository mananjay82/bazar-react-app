import MuiContainerHeader from "./MuiContainerHeader";
import MuiContainerTopBar from "./MuiContainerTopBar";

const Header = () => {
  return (
    <>
      <MuiContainerTopBar />
      <MuiContainerHeader />
    </>
  );
};

export default Header;
