import { useState } from "react";

import { Container, Box, Link, Select, MenuItem } from "@mui/material";
import CallIcon from "@mui/icons-material/Call";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
// import ExpandMoreIcon from '@mui/icons-material/ExpandMore';


import "./MuiContainerTopBar.css";

const MuiContainerTopBar = () => {
  const [language, setLanguage] = useState("EN");
  const [currency, setCurrency] = useState("USD");

  const languageChangeHandler = (event) => {
    setLanguage(event.target.value);
  };

  const currencyChangeHandler = (event) => {
    setCurrency(event.target.value);
  };

  return (
    <Box
      sx={{
        backgroundColor: "primary.main",
        fontSize: "12px",
        height: "40px",
        color: "#fff",
      }}
    >
      <Container
        sx={{
          display: "flex",
          height: "100%",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        {/* top left box */}

        <Box className='topBarLogoBox'
          sx={{
            display: "flex",
            marginRight: "16px",
            alignItems: "center",
            minWidth: "170px",
          }}
        >
          <Link href="/">
            <img
              src="https://bazar-react.vercel.app/assets/images/logo.svg"
              alt="logo"
            />
          </Link>
        </Box>

        <Box className='topBarLefLinks'
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <CallIcon />
            <Box component="span" sx={{ marginLeft: "10px" }}>
              +88012 3456 7894
            </Box>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              marginLeft: "20px",
            }}
          >
            <MailOutlineIcon />
            <Box component="span" sx={{ marginLeft: "10px" }}>
              support@ui-lib.com
            </Box>
          </Box>
        </Box>

        {/* top right box */}

        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Link
            className="topBarLink"
            href="/faq"
            color="inherit"
            underline="none"
            paddingRight="30px"
          >
            Theme FAQ"s
          </Link>
          <Link
            className="topBarLink"
            href="/help"
            color="inherit"
            underline="none"
            paddingRight="30px"
          >
            Need Help?
          </Link>
          {/* <Button variant='text' color='inherit' endIcon={<ExpandMoreIcon />}>EN</Button> */}
          <Select
            variant="standard"
            label="EN"
            value={language}
            onChange={languageChangeHandler}
            sx={{
              color: "white",
              fontSize: "12px",
              border: "none",
              outline: "none",
            }}
            className='topSelectBtn'
          >
            <MenuItem value="EN">EN</MenuItem>
            <MenuItem value="BN">BN</MenuItem>
            <MenuItem value="HN">HN</MenuItem>
          </Select>
          {/* <Button variant='text' color='inherit' endIcon={<ExpandMoreIcon />}>USD</Button> */}
          <Select
            variant="standard"
            label="USD"
            value={currency}
            onChange={currencyChangeHandler}
            sx={{
              color: "white",
              fontSize: "12px",
              border: "none",
              outline: "none",
            }}
            className='topSelectBtn'
          >
            <MenuItem value="USD">USD</MenuItem>
            <MenuItem value="EUR">EUR</MenuItem>
            <MenuItem value="BDT">BDT</MenuItem>
            <MenuItem value="INR">INR</MenuItem>
          </Select>
        </Box>
      </Container>
    </Box>
  );
};

export default MuiContainerTopBar;
