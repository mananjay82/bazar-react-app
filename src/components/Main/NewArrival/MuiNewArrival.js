import { Box, Grid } from "@mui/material";

import MuiNewArrivalCard from "./MuiNewArrivalCard";

const MuiNewArrival = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Grid container>
            <Grid item lg={12}>
                <MuiNewArrivalCard />
            </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default MuiNewArrival;
