import { Box, Typography, Link, Paper, Grid, CardContent, CardMedia } from "@mui/material";
import FiberNewIcon from '@mui/icons-material/FiberNew';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

const newArrivalProducts = [
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fimagegoggles.png&w=1920&q=75",
    title: "Sunglass",
    amount: 150
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(2).png&w=1920&q=75",
    title: "Makeup",
    amount: 250
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fbgwatch.png&w=1920&q=75",
    title: "Smart Watch",
    amount: 350
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(1).png&w=1920&q=75",
    title: "Lipstick",
    amount: 15
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(4).png&w=1920&q=75",
    title: "Green Plant",
    amount: 55
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flipstick%20(3).png&w=1920&q=75",
    title: "Bonsai tree",
    amount: 535
  },
];

const NewArrivalCard = (props) => {
  return (
    <Grid item lg={2} md={3} sm={4} xs={12}>
      <Link href='#'
        sx={{
          cursor: 'pointer',
          textDecoration: 'none',
      }}>
      <CardMedia
        component="img"
        height="185"
        image={props.image}
        alt={props.title}
        sx={{
          borderRadius: '10px',
          '&:hover': {
            filter: 'brightness(80%)'
          }
        }}
      />
      <CardContent>
        <Typography variant="body2" fontWeight='bold' sx={{color: 'secondary.light', fontSize: '14px', fontFamily: 'fontFamily' }}>{props.title}</Typography>
        <Typography variant="body2" fontWeight='600' color="#D23F57" >${props.amount}</Typography>
      </CardContent>
      </Link>
    </Grid>
  );
};



const MuiNewArrivalCard = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box>
            <FiberNewIcon fontSize="large"
              sx={{ color: "green", marginRight: "5px", marginBottom: "-5px" }}
            />
          </Box>
          <Typography
            variant="h5"
            fontWeight="bold"
            sx={{ color: "secondary.light", fontFamily: "fontFamily" }}
          >
            New Arrival
          </Typography>
        </Box>
        <Link
          href="#"
          sx={{
            textDecoration: "none",
          }}
        >
          <Box
            sx={{
              display: "flex",
            }}
          >
            View all
            <ArrowRightIcon />
          </Box>
        </Link>
      </Box>

      <Box>
        <Paper
          elevation={3}
          sx={{
            padding: "10px",
            marginTop: "10px",
            borderRadius: '5px'
          }}
        >
          <Grid container spacing={3} sx={{
            padding: '10px',
          }}>
            {
              newArrivalProducts.map((product, index) => {
                return (
                  <NewArrivalCard key={index.toString()} image={product.image} title={product.title} amount={product.amount} />
                )
              })
            }
          </Grid>
        </Paper>
      </Box>
    </Box>
  );
};

export default MuiNewArrivalCard;
