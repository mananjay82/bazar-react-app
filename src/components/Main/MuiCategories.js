import { Box, Typography, Grid, Link } from "@mui/material";
import WidgetsRoundedIcon from '@mui/icons-material/WidgetsRounded';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import MuiCategoryCard from "./MuiCategoryCard";

const Categories = [
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F7.DenimClassicBlueJeans.png&w=64&q=75",
    title: "Automobiles",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FClothes%2F20.GrayOvercoatWomen.png&w=64&q=75",
    title: "Car",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F8.IndianPearlThreadEarrings.png&w=64&q=75",
    title: "Fashion",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FJewellery%2F21.FeathersandBeadsBohemianNecklace.png&w=64&q=75",
    title: "Mobile",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FShoes%2F11.Flowwhite.png&w=64&q=75",
    title: "Laptop",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F1.SaktiSambarPowder.png&w=64&q=75",
    title: "Desktop",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F14.ACIProducts.png&w=64&q=75",
    title: "Tablet",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FGroceries%2F27.SardinesPack.png&w=64&q=75",
    title: "Fashion",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHealth%26Beauty%2F12.BeautySocietyantiacnemask.png&w=64&q=75",
    title: "Electronics",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHealth%26Beauty%2F25.MarioBadescuSkinCareShampoo.png&w=64&q=75",
    title: "Furniture",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FHome%26Garden%2F13.GardenRosesinBlueVase.png&w=64&q=75",
    title: "Camera",
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FFashion%2FAccessories%2F12.Xiaomimiband2.png&w=64&q=75",
    title: "Electronics",
  },
];

const MuiCategories = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}
      >
        <Typography variant="h5" fontWeight="bold" sx={{ color: 'secondary.light', fontFamily: 'fontFamily' }}>
          <WidgetsRoundedIcon sx={{ color: "#D23F57", marginRight: "5px", marginBottom: '-5px' }} />
          Categories
        </Typography>
        <Link
          href="#"
          sx={{
            textDecoration: "none",
          }}
        >
          <Box
            sx={{
              display: "flex",
            }}
          >
            View all
            <ArrowRightIcon />
          </Box>
        </Link>
      </Box>

      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "30px",
          height: "100%",
        }}
      >
        <Grid container spacing={3}>
          {Categories.map((category, index) => {
            return (
              <Grid item lg={2} md={4} sm={6} xs={12} key={index.toString()}>
                <MuiCategoryCard
                  image={category.image}
                  title={category.title}
                />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </Box>
  );
};

export default MuiCategories;
