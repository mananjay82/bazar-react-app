import { Typography, Box, Link } from "@mui/material";
import BoltOutlinedIcon from "@mui/icons-material/BoltOutlined";
import MuiCard from "./MuiCard";
import Carousel from "react-elastic-carousel";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

import './MuiFlashDealsSection.css';

const products = [
  {
    img: "https://bazar-react.vercel.app/assets/images/products/nike-black.png",
    title: "NikeCourt Zoom Vapour Cage",
    rating: 4,
    amount: 187.5,
    cancelledAmount: 250,
    discount: "25% off",
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fflash-2.png&w=1920&q=75",
    title: "Classic Rolex Watch",
    rating: 3,
    amount: 297.5,
    cancelledAmount: 350,
    discount: "15% off",
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fflash-3.png&w=1920&q=75",
    title: "Iphone 13 Pro Max",
    rating: 5,
    amount: 108.0,
    cancelledAmount: 150,
    discount: "28% off",
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fflash-4.png&w=1920&q=75",
    title: "MI LED Smart Watch",
    rating: 4,
    amount: 142.5,
    cancelledAmount: 180,
    discount: "21% off",
  },
  {
    img: "https://bazar-react.vercel.app/assets/images/products/nike-black.png",
    title: "NikeCourt Zoom Vapour Cage",
    rating: 4,
    amount: 187.5,
    cancelledAmount: 250,
    discount: "25% off",
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fflash-2.png&w=1920&q=75",
    title: "Classic Rolex Watch",
    rating: 3,
    amount: 297.5,
    cancelledAmount: 350,
    discount: "15% off",
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fflash-3.png&w=1920&q=75",
    title: "Iphone 13 Pro Max",
    rating: 5,
    amount: 108.0,
    cancelledAmount: 150,
    discount: "28% off",
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fflash-4.png&w=1920&q=75",
    title: "MI LED Smart Watch",
    rating: 4,
    amount: 142.5,
    cancelledAmount: 180,
    discount: "21% off",
  },
];

const breakPoints = [
  {
    width: 1,
    itemsToShow: 1,
  },
  {
    width: 450,
    itemsToShow: 2,
  },
  {
    width: 650,
    itemsToShow: 3,
  },
  {
    width: 950,
    itemsToShow: 4,
  },
  {
    width: 1300,
    itemsToShow: 5,
  },
];

const MuiFlashDealsSection = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}
      >
        <Typography
          variant="h5"
          fontWeight="bold"
          sx={{ color: "secondary.light", fontFamily: "fontFamily" }}
        >
          <BoltOutlinedIcon
            sx={{ color: "#D23F57", fontSize: "32px", marginBottom: "-5px" }}
          />
          Flash Deals
        </Typography>
        <Link
          href="#"
          sx={{
            textDecoration: "none",
          }}
        >
          <Box
            sx={{
              display: "flex",
            }}
          >
            View all
            <ArrowRightIcon />
          </Box>
        </Link>
      </Box>

      <Box
        sx={{
          width: "81%",
          textAlign: "left",
          paddingTop: "50px",
          height: "100%",
        }}
      >
        <Box className="flashCarousel">
          <Carousel itemsToScroll={1} itemsToShow={4} breakPoints={breakPoints}>
            {products.map((product, index) => {
              return (
                <MuiCard
                  img={product.img}
                  title={product.title}
                  rating={product.rating}
                  amount={product.amount}
                  cancelledAmount={product.cancelledAmount}
                  discount={product.discount}
                  key={index.toString()}
                />
              );
            })}
          </Carousel>
        </Box>
      </Box>
    </Box>
  );
};

export default MuiFlashDealsSection;
