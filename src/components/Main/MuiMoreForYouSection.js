import { Box, Typography, Grid, Link } from "@mui/material";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import MuiCard from "./MuiCard";

const moreForYouProducts = [
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F21.TarzT3.png&w=1920&q=75",
    title: "Tarz T3",
    rating: 4,
    amount: 164.70,
    cancelledAmount: 183.00,
    discount: '10% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F22.YamahaR15Black.png&w=1920&q=75",
    title: "Xamaha R15 Black",
    rating: 2,
    amount: 162.00,
    cancelledAmount: 180.00,
    discount: '10% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F23.YamahaR15Blue.png&w=1920&q=75",
    title: "Xamaha R15 Black",
    rating: 2,
    amount: 249.30,
    cancelledAmount: 277.00,
    discount: '10% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F24.Revel2020.png&w=1920&q=75",
    title: "Xevel 2020",
    rating: 2,
    amount: 256.50,
    cancelledAmount: 270.00,
    discount: '5% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FBikes%2F25.JacksonTB1.png&w=1920&q=75",
    title: "Jackson TB1",
    rating: 3,
    amount: 112.10,
    cancelledAmount: 118.00,
    discount: '5% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F1.Siri2020.png&w=1920&q=75",
    title: "Siri 2020",
    rating: 4,
    amount: 122.20,
    cancelledAmount: 130.00,
    discount: '6% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F2.COSOR1.png&w=1920&q=75",
    title: "COSOR1",
    rating: 4,
    amount: 273.60,
    cancelledAmount: 288.00,
    discount: '5% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F3.PanasonicCharge.png&w=1920&q=75",
    title: "Panasonic Charger",
    rating: 5,
    amount: 107.10,
    cancelledAmount: 119.00,
    discount: '10% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F3.PanasonicCharge.png&w=1920&q=75",
    title: "Lumic DSLR",
    rating: 5,
    amount: 115.32,
    cancelledAmount: 124.00,
    discount: '7% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F4.LumixDSLR.png&w=1920&q=75",
    title: "Atech Cam 1080p",
    rating: 3,
    amount: 264.60,
    cancelledAmount: 294.00,
    discount: '10% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F5.AtechCam1080p.png&w=1920&q=75",
    title: "Tony a9",
    rating: 5,
    amount: 233.70,
    cancelledAmount: 246.00,
    discount: '5% off'
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F6.Sonya9.png&w=1920&q=75",
    title: "beat sw3",
    rating: 3,
    amount: 112.53,
    cancelledAmount: 121.00,
    discount: '7% off'
  },
];

const MuiMoreForYouSection = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}
      >
        <Typography variant="h5" fontWeight="bold" sx={{ color: 'secondary.light', fontFamily: 'fontFamily' }}>
          More For You
        </Typography>
        <Link
          href="#"
          sx={{
            textDecoration: "none",
          }}
        >
          <Box
            sx={{
              display: "flex",
            }}
          >
            View all
            <ArrowRightIcon />
          </Box>
        </Link>
      </Box>

      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          height: "100%",
        }}
      >
        <Grid container spacing={3}>
          {moreForYouProducts.map((product, index) => {
            return (
              <Grid item lg={3} sm={6} xs={12} key={index.toString()}>
                <MuiCard
                  img={product.img}
                  title={product.title}
                  rating={product.rating}
                  amount={product.amount}
                  cancelledAmount={product.cancelledAmount}
                  discount={product.discount}
                />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </Box>
  );
};

export default MuiMoreForYouSection;
