import { Box, Container, Paper } from '@mui/material';
import Carousel from 'react-elastic-carousel';
import CarouselItem from './CarouselItems';
import MuiFlashDealsSection from './MuiFlashDealsSection';

const items = [
    {
        id: '1',
        img: 'https://bazar-react.vercel.app/assets/images/products/nike-black.png',
        title: '50% off For Your First Shopping',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
    },
    {
        id: '2',
        img: 'https://bazar-react.vercel.app/assets/images/products/nike-black.png',
        title: '50% off For Your First Shopping',
        subtitle: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
    }
]

const MuiPaperSection = () => {
    return (
        <Paper elevation={3}>
            <Box sx={{
                marginBottom: '60px'
            }}>
                <Container>
                    <Carousel showArrows={false}>
                        {
                            items.map((item, index) => {
                                return (
                                    <CarouselItem imgLink={item.img} title={item.title} subtitle={item.subtitle} key={index.toString()} />
                                )
                            })
                        }
                    </Carousel>
                </Container>
            </Box>
            <MuiFlashDealsSection />

        </Paper>
    )
}

export default MuiPaperSection;