import MuiCategories from "./MuiCategories";
import MuiContainerService from "./MuiContainerService";
import MuiMoreForYouSection from "./MuiMoreForYouSection";
import MuiPaperSection from "./MuiPaperSection";
import MuiContainerTopCategories from "./MuiContainerTopCategories";
import MuiRatingFeatured from "./Rating&Featured/MuiRatingFeatured";
import MuiNewArrival from './NewArrival/MuiNewArrival';
import MuiBigDiscounts from "./BigDiscounts/MuiBigDiscounts";


const Main = () => {
    return (
        <>
            <MuiPaperSection />
            <MuiContainerTopCategories />
            <MuiRatingFeatured />
            <MuiNewArrival />
            <MuiBigDiscounts />
            <MuiCategories />
            <MuiMoreForYouSection />
            <MuiContainerService />
        </>
    )
}

export default Main;