import { Stack, Box, Typography, Link } from "@mui/material";

const MuiCategoryCard = (props) => {
  return (
    <Link href='#' sx={{
      textDecoration: 'none'
    }}>
    <Stack spacing={1} direction="row" sx={{
        backgroundColor: 'white',
        alignItems: 'center',
        '&:hover': {
          boxShadow: '5px 5px 30px 5px rgb(3 0 71 / 9%)',
          transition: 'all 250ms ease-in-out'
        }
    }}>
      <Box>
        <img src={props.image} alt={props.title} />
      </Box>
      <Box>
        <Typography fontWeight='600'>{props.title}</Typography>
      </Box>
    </Stack>
    </Link>
  );
};

export default MuiCategoryCard;
