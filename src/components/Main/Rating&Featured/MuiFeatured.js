import { Box, Typography, Link, Paper, Grid, CardContent, CardMedia } from "@mui/material";
import StarsIcon from "@mui/icons-material/Stars";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

const featuredProducts = [
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Flondon-britches.png&w=1920&q=75",
    title: "London Britches"
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fjim%20and%20jiko.png&w=1920&q=75",
    title: "Jim & Jago"
  },
];

const FeaturedCard = (props) => {
  return (
    <Grid item lg={6} md={12} sm={12} xs={12}>
      <Link href='#'
        sx={{
          cursor: 'pointer',
          textDecoration: 'none',
      }}>
      <CardMedia
        component="img"
        height="185"
        image={props.image}
        alt={props.title}
        sx={{
          borderRadius: '3px',
          '&:hover': {
            filter: 'brightness(80%)'
          }
        }}
      />
      <CardContent>
        <Typography variant="body2" fontWeight='bold' sx={{color: 'secondary.light', fontSize: '14px', fontFamily: 'fontFamily' }}>{props.title}</Typography>
      </CardContent>
      </Link>
    </Grid>
  );
};



const MuiFeatured = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box>
            <StarsIcon
              sx={{ color: "orange", marginRight: "5px", marginBottom: "-5px" }}
            />
          </Box>
          <Typography
            variant="h5"
            fontWeight="bold"
            sx={{ color: "secondary.light", fontFamily: "fontFamily" }}
          >
            Featured Brands
          </Typography>
        </Box>
        <Link
          href="#"
          sx={{
            textDecoration: "none",
          }}
        >
          <Box
            sx={{
              display: "flex",
            }}
          >
            View all
            <ArrowRightIcon />
          </Box>
        </Link>
      </Box>

      <Box>
        <Paper
          elevation={3}
          sx={{
            padding: "10px",
            marginTop: "10px",
            borderRadius: '5px'
          }}
        >
          <Grid container spacing={2} sx={{
            padding: '10px',
          }}>
            {
              featuredProducts.map((product, index) => {
                return (
                  <FeaturedCard key={index.toString()} image={product.image} title={product.title}  />
                )
              })
            }
          </Grid>
        </Paper>
      </Box>
    </Box>
  );
};

export default MuiFeatured;
