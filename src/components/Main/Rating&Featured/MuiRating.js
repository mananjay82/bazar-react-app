import { Box, Typography, Link, Paper, Grid, Rating, CardMedia, CardContent } from "@mui/material";
import StarsIcon from "@mui/icons-material/Stars";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

const topRatingProducts = [
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fcamera-1.png&w=1920&q=75",
    title: "Camera",
    rating: 4,
    amount: "3300",
    reviews: 49
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fshoes-2.png&w=1920&q=75",
    title: "Shoe",
    rating: 5,
    amount: "400",
    reviews: 20
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fmobile-1.png&w=1920&q=75",
    title: "Phone",
    rating: 5,
    amount: "999",
    reviews: 65
  },
  {
    image:
      "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2Fwatch-1.png&w=1920&q=75",
    title: "Watch",
    rating: 5,
    amount: "999",
    reviews: 75
  },
];

const RatingCard = (props) => {
  return (
    <Grid item lg={3} md={4} sm={6} xs={12} >
      <Link href='#'
        sx={{
          cursor: 'pointer',
          textDecoration: 'none',
      }}>
      <CardMedia
        component="img"
        height="144"
        image={props.image}
        alt={props.title}
        sx={{
            borderRadius: '3px',
            textAlign: 'center',
            alignItems: 'center',
            '&:hover': {  
              backgroundColor: 'rgba(106, 110, 106, 0.7)'  /*filter: 'brightness(50%)'*/ 
            }
        }}
      />
      <CardContent sx={{textAlign: 'center'}}>
        <Box sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
        }}>
        <Rating name="read-only" value={props.rating} readOnly={true} size='small' />
        <Typography variant="body2" fontWeight='bold' sx={{color: 'secondary.light', fontSize: '10px', fontFamily: 'fontFamily' }}>({props.reviews})</Typography>
        </Box>
        <Typography variant="body2" fontWeight='bold' sx={{color: 'secondary.light', fontSize: '14px', fontFamily: 'fontFamily' }}>{props.title}</Typography>
            <Typography variant="body1" color="#D23F57" >
              ${props.amount}
            </Typography>
      </CardContent>
      </Link>

    </Grid>
  );
};

const MuiRating = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box>
            <StarsIcon
              sx={{ color: "orange", marginRight: "5px", marginBottom: "-5px" }}
            />
          </Box>
          <Typography
            variant="h5"
            fontWeight="bold"
            sx={{ color: "secondary.light", fontFamily: "fontFamily" }}
          >
            Top Rating
          </Typography>
        </Box>
        <Link
          href="#"
          sx={{
            textDecoration: "none",
          }}
        >
          <Box
            sx={{
              display: "flex",
            }}
          >
            View all
            <ArrowRightIcon />
          </Box>
        </Link>
      </Box>

      <Box>
        <Paper
          elevation={3}
          sx={{
            padding: "10px",
            marginTop: "10px",
            borderRadius: '5px'
          }}
        >
          <Grid container spacing={2} sx={{
            padding: '10px'
          }}>
            {topRatingProducts.map((product, index) => {
              return (
                <RatingCard
                  key={index.toString()}
                  image={product.image}
                  title={product.title}
                  amount={product.amount}
                  rating={product.rating}
                  reviews={product.reviews}
                />
              );
            })}
          </Grid>
        </Paper>
      </Box>
    </Box>
  );
};

export default MuiRating;
