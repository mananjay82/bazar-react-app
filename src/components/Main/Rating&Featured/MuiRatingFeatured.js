import { Box, Grid } from "@mui/material";
import MuiFeatured from "./MuiFeatured";
import MuiRating from "./MuiRating";

const MuiRatingFeatured = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Grid container spacing={5}>
          <Grid item lg={6} md={12}>
            <MuiRating />
          </Grid>
          <Grid item lg={6} md={12}>
            <MuiFeatured />
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};

export default MuiRatingFeatured;
