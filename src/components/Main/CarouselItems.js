import { Typography, Button, Grid } from "@mui/material";

const CarouselItem = (props) => {
  return (
    // <Stack spacing={4} direction='row' alignItems='center' justifyContent='center'>
    //     <Box sx={{
    //         textAlign: 'left'
    //     }}>
    //         <Typography variant='h2' fontWeight='bold' fontFamily='fontFamily' sx={{color: 'secondary.light'}}>{props.title}</Typography>
    //         <Typography variant='body2' color='rgba(0, 0, 0, 0.54)'>{props.subtitle}</Typography>
    //         <Button variant='contained' sx={{
    //             backgroundColor: '#D23F57',
    //             marginTop: '25px',
    //         }}>Shop Now</Button>
    //     </Box>
    //     <Box>
    //         <img src={props.imgLink} alt={props.text} width='500px' />
    //     </Box>
    // </Stack>

    <Grid container alignItems='center'>
      <Grid item lg={6} md={12}
        sx={{
          textAlign: "left",
        }}
      >
        <Typography
          variant="h2"
          fontWeight="bold"
          fontFamily="fontFamily"
          sx={{ color: "secondary.light" }}
        >
          {props.title}
        </Typography>
        <Typography variant="body2" color="rgba(0, 0, 0, 0.54)">
          {props.subtitle}
        </Typography>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#D23F57",
            marginTop: "25px",
          }}
        >
          Shop Now
        </Button>
      </Grid>
      <Grid item lg={6} md={12}>
        <img src={props.imgLink} alt={props.text} width="500px" />
      </Grid>
    </Grid>
  );
};

export default CarouselItem;
