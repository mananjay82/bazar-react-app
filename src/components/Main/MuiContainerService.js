import { Grid, Box } from "@mui/material";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import CreditScoreOutlinedIcon from "@mui/icons-material/CreditScoreOutlined";
import GppGoodOutlinedIcon from "@mui/icons-material/GppGoodOutlined";
import SupportAgentOutlinedIcon from "@mui/icons-material/SupportAgentOutlined";

import MuiServiceCard from "./MuiServiceCard";

const services = [
  {
    icon: LocalShippingOutlinedIcon,
    title: "Worldwide Delivery",
    subtitle:
      "We offer competitive prices on our 100 million plus product any range.",
  },
  {
    icon: CreditScoreOutlinedIcon,
    title: "Safe Payment",
    subtitle:
      "We offer competitive prices on our 100 million plus product any range.",
  },
  {
    icon: GppGoodOutlinedIcon,
    title: "Shop With Confidence",
    subtitle:
      "We offer competitive prices on our 100 million plus product any range.",
  },
  {
    icon: SupportAgentOutlinedIcon,
    title: "24/7 Support",
    subtitle:
      "We offer competitive prices on our 100 million plus product any range.",
  },
];

const MuiContainerService = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
        marginBottom: '50px'
      }}
    >
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          height: "100%",
        }}
      >
        <Grid container spacing={4}>
          {services.map((service, index) => {
            return (
              <Grid item key={index.toString()} lg={3} sm={6} xs={12}>
                <MuiServiceCard
                  icon={service.icon}
                  title={service.title}
                  subtitle={service.subtitle}
                />
              </Grid>
            );
          })}
        </Grid>
      </Box>
    </Box>
  );
};

export default MuiContainerService;
