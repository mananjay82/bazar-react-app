import { Box, Link, Typography } from "@mui/material";
import Carousel from "react-elastic-carousel";
import WidgetsRoundedIcon from "@mui/icons-material/WidgetsRounded";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import MuiTopCategoriesCard from "./MuiTopCategoriesCard";

import './MuiContainerTopCategories.css';

const topCategoriesItems = [
    {
        image: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-1.png&w=2048&q=75',
        label1: 'Headephones',
        label2: '3k orders this week'
    },
    {
        image: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-2.png&w=1920&q=75',
        label1: 'Watch',
        label2: '3k orders this week'
    },
    {
        image: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-3.png&w=1920&q=75',
        label1: 'Sunglass',
        label2: '3k orders this week'
    },
    {
        image: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-1.png&w=2048&q=75',
        label1: 'Headephones',
        label2: '3k orders this week'
    },
    {
        image: 'https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fbanners%2Fcategory-2.png&w=1920&q=75',
        label1: 'Watch',
        label2: '3k orders this week'
    },
];

const breakPoints = [
    
    {
        width: 650,
        itemsToShow: 1,
    },
    {
        width: 950,
        itemsToShow: 3,
    },
    { 
        width: 1300, 
        itemsToShow: 3 
    },
  ];


const MuiContainerTopCategories = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Box
        sx={{
            width: "80%",
            textAlign: "left",
            paddingTop: "50px",
            display: 'flex',
            justifyContent: 'space-between'
          }}
      >
        <Box
          sx={{
            display: "flex",
            alignItems: 'center'
          }}
        >
          <Box>
            <WidgetsRoundedIcon sx={{ color: "#D23F57", marginRight: "5px", marginBottom: '-5px' }} />
          </Box>
          <Typography variant="h5" fontWeight="bold" sx={{ color: 'secondary.light', fontFamily: 'fontFamily' }}>Top Categories</Typography>
        </Box>
        <Link href='#' sx={{
          textDecoration: 'none'
        }}>
          <Box
            sx={{
              display: "flex",
            }}
          >
            View All <ArrowRightIcon />
          </Box>
        </Link>
      </Box>
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "30px",
          height: "100%",
        }}
      >     
            <Box className="carousel">
            <Carousel breakPoints={breakPoints}>
            {
                topCategoriesItems.map((item, index) => {
                    return (
                        <MuiTopCategoriesCard image={item.image} label1={item.label1} label2={item.label2} key={index.toString()} />
                    )
                })
            }
            </Carousel>
            </Box>
      </Box>
    </Box>
  );
};

export default MuiContainerTopCategories;
