import {
  Link,
  Paper,
  Chip,
  Box,
  Card,
  CardMedia,
} from "@mui/material";

const MuiTopCategoriesCard = (props) => {
  return (
    <Link href='#'>
      <Paper
        elevation={0}
        sx={{
          padding: "16px",
          borderRadius: "8px",
          color: "#2B3445",
          overflow: "unset",
          transition: "all 250ms ease-in-out",
          marginLeft: "15px",
          marginRight: "15px",
          position: 'relative'
        }}
      >
        {/* <Paper
          elevation={1}
          sx={{
            borderRadius: "8px",
            boxShadow: "0px 1px 3px rgb(3 0 71 / 9%)",
            position: "relative",
            color: '#2B3445'
          }}
        >
          <Chip label={props.label1} sx={{
            height: '24px',
            color: '#2B3445',
            zIndex: 2,
            position: 'absolute',
            fontSize: '10px',
            padding: '0 8px',
            fontWeight: '600',
            top: '0.875rem',
            left: '0.875rem',
            boxSizing: 'border-box'
          }}></Chip>
          <Chip label={props.label2} sx={{
            height: '24px',
            color: '#2B3445',
            zIndex: 2,
            position: 'absolute',
            fontSize: '10px',
            padding: '0 8px',
            fontWeight: '600',
            top: '0.875rem',
            right: '0.875rem',
            boxSizing: 'border-box'
          }}></Chip>
          <Box
            sx={{
              overflow: "hidden",
              height: "120px",
              position: "relative",
              borderRadius: "8px",
            }}
          >
            <Box component="span" sx={{
                boxSizing: 'border-box',
                display: 'block',
                overflow: 'hidden',
                width: 'initial',
                height: 'initial',
                opacity: '1',
                position: 'absolute',
                top: 0,
                left: 0,
                bottom: 0,
                right: 0,
                padding: 0,
                border: 0,
                margin: 0,
                background: 'none'
            }}>
              <img
                src={props.image}
                alt={props.label1}
                style={{
                  objectFit: "cover",
                  maxHeight: "100%",
                  maxWidth: "100%",
                  position: "absolute",
                  top: 0,
                  left: 0,
                  bottom: 0,
                  right: 0,
                  display: "block",
                }}
              />
            </Box>
          </Box>
        </Paper> */}

        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Chip
            label={props.label1}
            sx={{
              textDecoration: "none",
              position: "absolute",
              left: "1.5rem",
              top: '1.5rem',
              backgroundColor: "#0F3460",
              color: "#fff",
              fontSize: "10px",
              padding: '0 8px',
              fontWeight: 600,
              cursor: 'pointer'
            }}
          />
          <Chip
            label={props.label2}
            sx={{
              textDecoration: "none",
              position: "absolute",
              right: "1.5rem",
              top: '1.5rem',
              fontSize: '10px',
              color: '#2B3445',
              fontWeight: 600,
              cursor: 'pointer'
            }}
          />
        </Box>

        <Card>
          <CardMedia component="img" image={props.image} alt={props.label1} />
        </Card>
      </Paper>
    </Link>
  );
};

export default MuiTopCategoriesCard;
