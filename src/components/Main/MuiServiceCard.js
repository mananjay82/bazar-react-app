import { CardContent, Typography, Button, Card } from "@mui/material";
// import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";

const MuiServiceCard = (props) => {
  return (
    <Card sx={{
      height: '200px',
      padding: '30px',
      borderRadius: '10px',
      '&:hover': {
        boxShadow: '5px 5px 30px 5px rgb(3 0 71 / 9%)',
        transition: 'all 250ms ease-in-out'
      }
    }}>
      <CardContent sx={{
        textAlign: 'center'
      }}>
        <Button sx={{
          margin: '0 auto',
          padding: '15px',
          backgroundColor: '#F3F5F9',
          color: 'rgba(0, 0, 0, 0.54)',
          borderRadius: '50%'
        }}>
          <props.icon sx={{
            fontSize: '30px',
            color: 'rgba(0, 0, 0, 0.54)'
          }}/>
        </Button>
        <Typography variant="h6" sx={{
          marginTop: '15px',
          marginBottom: '10px',
          color: 'secondary.light',
          fontFamily: 'fontFamily',
          fontSize: '18px'
        }}>{props.title}</Typography>
        <Typography variant="body2" sx={{
          color: '#7D879C',
          fontFamily: 'fontFamily'
        }}>
          {props.subtitle}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default MuiServiceCard;
