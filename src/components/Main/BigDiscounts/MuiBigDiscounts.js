import { Typography, Box, Link } from "@mui/material";
import RedeemOutlinedIcon from '@mui/icons-material/RedeemOutlined';
import BigDiscountsCard from './BigDiscountsCard';
import Carousel from "react-elastic-carousel";
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

// import './MuiFlashDealsSection.css';

const products = [
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F7.beatsw3.png&w=1920&q=75",
    title: "BenX 2020",
    amount: 233,
    cancelledAmount: 233,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F9.SonyTV1080p.png&w=1920&q=75",
    title: "Sony TV 1080p",
    amount: 278,
    cancelledAmount: 278,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F10.SonyPS4.png&w=1920&q=75",
    title: "Sony PS4",
    amount: 177,
    cancelledAmount: 177,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F11.Netgear2020.png&w=1920&q=75",
    title: "Setgearr 2020",
    amount: 124,
    cancelledAmount: 124,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F12.SonyBGB.png&w=1920&q=75",
    title: "Sony BGB",
    amount: 284,
    cancelledAmount: 284,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F13.LGProducts.png&w=1920&q=75",
    title: "RG Products",
    amount: 300,
    cancelledAmount: 300,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F14.Panasonic2019.png&w=1920&q=75",
    title: "Pansonic 2019",
    amount: 137,
    cancelledAmount: 137,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F15.DuneHD.png&w=1920&q=75",
    title: "Pune HD",
    amount: 111,
    cancelledAmount: 111,
  },
  {
    img: "https://bazar-react.vercel.app/_next/image?url=%2Fassets%2Fimages%2Fproducts%2FElectronics%2F16.SonyCCTV.png&w=1920&q=75",
    title: "Sony CCTV",
    amount: 150,
    cancelledAmount: 150,
  },
];

const breakPoints = [
  {
    width: 1,
    itemsToShow: 1,
  },
  {
    width: 450,
    itemsToShow: 2,
  },
  {
    width: 650,
    itemsToShow: 3,
  },
  {
    width: 950,
    itemsToShow: 6,
  },
];

const MuiBigDiscounts = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        backgroundColor: "#F6F9FC",
        alignItems: "center",
        height: "100%",
      }}
    >
      <Box
        sx={{
          width: "80%",
          textAlign: "left",
          paddingTop: "50px",
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}
      >
        <Typography
          variant="h5"
          fontWeight="bold"
          sx={{ color: "secondary.light", fontFamily: "fontFamily" }}
        >
          <RedeemOutlinedIcon
            sx={{ color: "#D23F57", fontSize: "32px", marginBottom: "-6px", marginRight: '5px' }}
          />
          Big Discounts
        </Typography>
        <Link
          href="#"
          sx={{
            textDecoration: "none",
          }}
        >
          <Box
            sx={{
              display: "flex",
            }}
          >
            View all
            <ArrowRightIcon />
          </Box>
        </Link>
      </Box>

      <Box
        sx={{
          width: "85%",
          textAlign: "left",
          paddingTop: "30px",
          height: "100%",
        }}
      >
        <Box className="flashCarousel">
          <Carousel itemsToScroll={1} itemsToShow={4} breakPoints={breakPoints}>
            {products.map((product, index) => {
              return (
                <BigDiscountsCard
                  img={product.img}
                  title={product.title}
                  rating={product.rating}
                  amount={product.amount}
                  cancelledAmount={product.cancelledAmount}
                  discount={product.discount}
                  key={index.toString()}
                />
              );
            })}
          </Carousel>
        </Box>
      </Box>
    </Box>
  );
};

export default MuiBigDiscounts;
