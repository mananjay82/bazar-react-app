import {
    CardContent,
    Card,
    Typography,
    CardMedia,
    Box,
    Link
  } from "@mui/material";

  
  const MuiCard = (props) => {
    return (
      <Card sx={{
        width: '92%'
      }}>
        <Link href='#'
          sx={{
            cursor: 'pointer',
            textDecoration: 'none',
        }}>
        <CardMedia
          component="img"
          height="184"
          image={props.img}
          alt={props.title}
          sx={{
            width: '90%',
            margin: '10px auto',
            borderRadius: '5px',
            '&:hover': {
                backgroundColor: 'rgba(143, 148, 143, 0.7)',
            }
          }}
        />
        <CardContent>
          <Typography variant="body2" fontWeight='bold' sx={{color: 'secondary.light', fontSize: '14px', fontFamily: 'fontFamily' }}>{props.title}</Typography>
          <Box sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}>
            <Box sx={{
              display: 'flex',
            }}>
              <Typography variant="body1" color="error">
                ${props.amount}
              </Typography>
              <Typography variant="body1" marginLeft='5px'>
                {props.cancelledAmount}
              </Typography>
            </Box>
          </Box>
        </CardContent>
        </Link>
      </Card>
    );
  };
  
  export default MuiCard;
  