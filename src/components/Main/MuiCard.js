import {
  CardContent,
  Card,
  Typography,
  CardHeader,
  CardMedia,
  Chip,
  Rating,
  Box,
  Button,
  Link
} from "@mui/material";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import AddBoxOutlinedIcon from '@mui/icons-material/AddBoxOutlined';

const MuiCard = (props) => {
  return (
    <Card sx={{
      width: '95%',
      '&:hover': {
        boxShadow: '5px 5px 30px 5px rgb(3 0 71 / 9%)',
        transition: 'all 250ms ease-in-out'
      }
    }}>
      <Link href='#'
        sx={{
          cursor: 'pointer',
          textDecoration: 'none',
      }}>
      <CardHeader
        avatar={<Chip label={props.discount} sx={{backgroundColor: '#D23F57', color: 'white', paddingLeft: '6px', paddingRight: '6px', fontSize: '10px' }} />}
        action={<FavoriteBorderOutlinedIcon />}
      />
      <CardMedia
        component="img"
        height="194"
        image={props.img}
        alt={props.title}
      />
      <CardContent>
        <Typography variant="body2" fontWeight='bold' sx={{color: 'secondary.light', fontSize: '14px', fontFamily: 'fontFamily' }}>{props.title}</Typography>
        <Rating name="read-only" value={props.rating} readOnly={true} />
        <Box sx={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}>
          <Box sx={{
            display: 'flex',
          }}>
            <Typography variant="body1" color="error">
              ${props.amount}
            </Typography>
            <Typography variant="body1" marginLeft='5px'>
              {props.cancelledAmount}
            </Typography>
          </Box>
          <Box>
            <Button sx={{
              padding: '3px'
            }}>
              <AddBoxOutlinedIcon color="error" sx={{
                fontSize: '28px'
              }}/>
            </Button>
          </Box>
        </Box>
      </CardContent>
      </Link>
    </Card>
  );
};

export default MuiCard;
