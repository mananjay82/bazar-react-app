import MuiFooterSection from "./MuiFooterSection";

const Footer = () => {
    return (
        <>
            <MuiFooterSection />
        </>
    )
}

export default Footer;