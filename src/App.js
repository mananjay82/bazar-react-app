import "./App.css";
import { createTheme, ThemeProvider } from "@mui/material";

import Header from './components/Header/Header';
import Main from "./components/Main/Main";
import Footer from "./components/Footer/Footer";

const theme = createTheme({
  palette: {
    primary: {
      main: '#0F3460'
    },
    secondary: {
      main: '#0c0e30',
      light: '#2B3445',
      dark: '#AEB4BE'
    },
  },
  typography: {
    fontFamily: 'Open Sans,Roboto,-apple-system,BlinkMacSystemFont,Segoe UI,Oxygen,Ubuntu,Cantarell,Fira Sans,Droid Sans,Helvetica Neue,sans-serif'
  }
}) 

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <Header />
        <Main />
        <Footer />
      </div>
    </ThemeProvider>
  );
}

export default App;
